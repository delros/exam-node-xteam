var fs = require('fs');
var path = require('path');

/**
 * Getting the parsed JSON value
 * @param path {String} The file path
 * @param defaultValue {Object} The default value of the parsed object
 * @param callback {Function} Success callback
 * @param callbackNotExists {Function} Notificattion callback in case if file not exists
 * @param callbackIsInvalid {Function} Notificattion callback in case if file has not valid JSON
 * @reruen {Object}
 */
function getJSONFromFile(path, defaultValue, callback, callbackNotExists, callbackIsInvalid) {
  fs.exists(path, function (exists) {
    if (!exists) {
      callbackNotExists && callbackNotExists(path);
      return callback(defaultValue);
    }

    fs.readFile(path, function (error, data) {
      if (error) throw error;
      var parsed = defaultValue;
      
      try {
        parsed = JSON.parse(data);
      } catch (e) {
        callbackIsInvalid && callbackIsInvalid(path);
      }

      return callback(parsed);
    });
  });
}

/**
 * Recursive search for teh files in scope of passed directory
 * @param directory {String} The root directory for lookup
 * @param done {Function} The callback for compvared lookup
 * @return {Array}
 */
function lookupFiles(directory, done) {
  var results = [];

  fs.readdir(directory, function (error, items) {
    if (error) {
      return done(error);
    }

    var pending = items.length;

    if (!pending) {
      return done(null, results);
    }

    items.forEach(function (file) {
      file = path.resolve(directory, file);

      fs.stat(file, function (error, stat) {
        if (stat && stat.isDirectory()) {
          return lookupFiles(file, function (error, res) {
            results = results.concat(res);
            if (!--pending) {
              done(null, results);
            }
          });
        }

        results.push(file);
        if (!--pending) {
          done(null, results);
        }
      });

    });
    
  });
}

/**
 * Line-by-line file reader
 * @param file {String} The path to the file 
 * @param callback {Function} 
 * @return {Object} Stream
 */
function readFileByLines(file, callback) {
  var remaining = '';
  var stream = fs.createReadStream(file);

  stream.on('data', function(data) {
    remaining += data;
    var index = remaining.indexOf('\n');
    while (index > -1) {
      var line = remaining.substring(0, index).replace(/\r/gm, '');
      remaining = remaining.substring(index + 1);
      callback(line);
      index = remaining.indexOf('\n');
    }
  });
 
  stream.on('end', function() {
    if (remaining.length > 0) {
      callback(remaining);
    }
  });

  return stream;
}

/**
 * Tags lookup in scope of passed target object
 * @param tags {Array} The list of tags for lookup
 * @param target {Object} The target for lookup
 * @return {Object}
 */
function lookupTags(tags, target) {
  var results = {};

  tags.forEach(function (tag) {
    results[tag] = 0;
  });

  if (!target) return results;

  Object.keys(target).forEach(function (key) {
    var item = target[key];

    switch (typeof item) {
      case 'string' :
        if (tags.indexOf(item) !== -1) {
          results[item] += 1;
        }
        break;
      case 'object' : 
        var nestedLookup = lookupTags(tags, item);
        Object.keys(nestedLookup).forEach(function (tag) {
          if (nestedLookup[tag] === 0) return;
          results[tag] += nestedLookup[tag];
        });
        break
    }

  });

  return results;
}

/**
 * Increasing the count values of tags which has been found
 * @param base {Object} 
 * @param increase {Object}
 * @return {Object}
 */
function increaseLookupResults(base, increase) {
  Object.keys(increase).forEach(function (key) {
    if (!base[key]) base[key] = 0;
    base[key] += increase[key];
  });
  return base;
}

/**
 * Helper for string rendering
 * @param pattern {String} The template string for rendering
 * @param data {Object} The container with data which should be rendered
 * @return {String}
 */
function stringUglyRender(pattern, data) {
  Object.keys(data).forEach(function (key) {
    var regexp = new RegExp('\\{{' + key + '\\}}', 'gm');
    pattern = pattern.replace(regexp, data[key]);
  });

  return pattern;
}


module.exports = {
  getJSONFromFile : getJSONFromFile,
  readFileByLines : readFileByLines,
  increaseLookupResults : increaseLookupResults,
  lookupFiles : lookupFiles,
  lookupTags : lookupTags,
  stringRender : stringUglyRender
};