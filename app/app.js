var path = require('path');
var helpers = require('./helpers');
var tagsDefinitions = [];
var tagsRenderingTemplate = '{{tag}} - {{count}}';

function notifyInvalidJSONFormat(file) {
	console.error('[warn] Invalid JSON format in ' + file);
}

function showResults(tags, pattern) {
	var tagsCountArray = [];

	Object.keys(tags).forEach(function (key) {
		tagsCountArray.push({
			tag : key,
			count : tags[key]
		});
	});

	tagsCountArray.sort(function(current, next){
	  return next.count - current.count
	}).forEach(function (item) {
		console.log(helpers.stringRender(pattern, item));
	});
}

helpers.readFileByLines('./tags.txt', function (data) {
  tagsDefinitions.push(data);
}).on('end', function () {
	helpers.lookupFiles('./data', function (error, items) {
		if (error) throw error;

		var pending = items.length;
		var tagsCount = {};

		items.forEach(function (item, index) {
			helpers.getJSONFromFile(item, {}, function (data) {
				var tagsLookupResults = helpers.lookupTags(tagsDefinitions, data);
				tagsCount = helpers.increaseLookupResults(tagsCount, tagsLookupResults);

				if (!--pending) {
					showResults(tagsCount, tagsRenderingTemplate);
				}

			}, null, notifyInvalidJSONFormat)
		});
	});
});